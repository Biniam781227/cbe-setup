import helmet from 'helmet'
import { allowed_urls } from '../config/default.json'

function helmetEncapsulation(app) {
    // helmet Encapsulation Policy Implemented to secure the Incomming headers');
    // app.use(helmet.contentSecurityPolicy())
    app.use(helmet.dnsPrefetchControl())
    // // app.use(helmet.expectCt()); //deprecated
    // app.use(helmet.frameguard())
    app.use(helmet.hidePoweredBy())
    app.use(helmet.hsts())
    app.use(helmet.ieNoOpen())
    app.use(helmet.noSniff())
    app.use(helmet.permittedCrossDomainPolicies())
    app.use(helmet.xssFilter())
    app.use(helmet.hidePoweredBy({ setTo: 'PHP 6.9.0' }))
    // app.use(
    //     helmet({
    //         referrerPolicy: {
    //             policy: 'no-referrer',
    //         },
    //     })
    // )
    // app.use(
    //     helmet({
    //         contentSecurityPolicy: {
    //             directives: {
    //                 'connect-src': ["'self'", ...allowed_urls],
    //                 'default-src': "'self'",
    //                 'frame-ancestors': ["'self'", ...allowed_urls],
    //                 sandbox: ['allow-forms', 'allow-scripts'],
    //                 'script-src': ["'self'", ...allowed_urls],
    //             },
    //         },
    //     })
    // )
    // ..........
}
export default helmetEncapsulation
