import routeUser from '../src/helpers/routeUser'
// import Notification from '../src/models/notificationModel';

async function handleSocketConnection(socket) {
    socket.join(socket.data.userId)
    socket.broadcast.emit('User_Connected')
    socket.emit('connected')
    let user = routeUser(socket.data.role)
    await user.update(socket.data.userId, { socket: socket.id })
    socket.resources = {
        screen: false,
        video: true,
        audio: true,
    }
    socket.join(socket.data.userId)
    socket.broadcast.emit('User_Connected')

    socket.on('disconnect', (data) => {
        user = routeUser(socket.data.role)
        console.log('user disconnected ', socket.id)
        // removeFeed()
        user.updateByCondition({ socket: socket.id }, { socket: '' })
            .then((doc) => {
                socket.broadcast.emit('User_Disconnected')
            })
            .catch((error) => {
                console.log(error)
            })
    })
}

export default handleSocketConnection
