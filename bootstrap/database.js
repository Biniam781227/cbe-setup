import mongoose from 'mongoose'
import config from 'config'
import startup from '../src/jobs/startup'

class Connection {
    constructor() {
        const url = `${process.env.DB_HOST}:${process.env.DB_PORT}/${process.env.DB_NAME}`

        console.log('Establish new connection with url', url)
        mongoose.Promise = global.Promise
        mongoose.set('useNewUrlParser', true)
        mongoose.set('useFindAndModify', false)
        // mongoose.set("useCreateIndex", true);
        // mongoose.set("autoIndex", true);
        mongoose.set('useUnifiedTopology', true)
        mongoose.connect(url, async (error) => {
            try {
                // EXECUTE STARTUP PROCESSES())
                await startup()
                // console.log('super admin account initialized');
            } catch (error) {
                console.log(error.message || 'failed to initialize superadmin')
            }
            console.log(error || 'connected to db successfully !!')
        })
    }
}

export default new Connection()
