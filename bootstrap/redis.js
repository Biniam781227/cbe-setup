import dotenv from 'dotenv'
import config from 'config'
const socketIO = require('socket.io')
const { createAdapter } = require('socket.io-redis')
import { Emitter } from '@socket.io/redis-emitter'
import { createClient } from 'redis'
import handleSocketConnection from './socket'
import socket_middleware from '../src/middlewares/socket_middleware'
dotenv.config()

export default async function initRedisSocket(server) {
    const io = socketIO(server, {
        cors: {
            origin: config.get('allowed_urls'),
            methods: ['GET', 'POST'],
            allowedHeaders: ['token'],
            credentials: true,
            allowEIO3: true,
        },
    })

    // Create a Redis client
    const redisClient = createClient({ url: 'redis://localhost:6379' })
    redisClient.on('connect', () => {
        console.log('Connected to Redis')
        // Start using Redis client here
    })
    redisClient.on('error', (err) => {
        console.error('Redis connection error:', err)
    })
    // Set up the socket.io-redis adapter
    // io.adapter(createAdapter({ url: 'redis://localhost:6379' }))

    // io.use((socket, next) => {
    //     // Custom middleware function for socket.io
    //     console.log('Socket.io middleware executed')
    //     next()
    // })

    io.use(socket_middleware)

    io.on('connect', (socket) => {
        console.log('A user connected')
        handleSocketConnection(socket)

        socket.on('disconnect', () => {
            console.log('A user disconnected')
        })
    })

    // Emit a socket
    function emitSocket(socketId, event, data) {
        io.to(socketId).emit(event, data)
    }

    // Create a Redis client for the emitter
    const emitter = new Emitter(redisClient)
    global.emitter = emitter
    global.io = io
    // Add middleware to the socket.io emitter
    // emitter.use((packet, next) => {
    //     // Custom middleware function for the socket.io emitter
    //     console.log('Socket.io emitter middleware executed')
    //     next()
    // })

    redisClient.connect().then(() => {
        setInterval(() => {
            emitter.emit('time', new Date())
        }, 5000)
    })
}
