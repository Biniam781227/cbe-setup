import { _ as e, o as t, c as a } from './chunks/framework.RpFkmAtX.js';
const u = JSON.parse(
    `{"title":"","description":"","frontmatter":{"layout":"home","hero":{"name":"Infinity Billing","text":"","tagline":"Revolutionizing E-School Management with CBE's Integrated Fee, Payroll and School Management System"},"features":[{"title":"Simplified Fee Management","details":"Say goodbye to long queues and manual fee collection processes. Our integrated system allows parents or students to conveniently pay school fees using the bank’s payment channels, ensuring a seamless transaction experience that saves time and effort"},{"title":"Streamlined Administrative","details":"Henon automates various administrative tasks, such as student admissions, attendance tracking, timetable creation, and exam management. This automation saves time and reduces manual errors, allowing administrators to focus on more strategic activities"},{"title":"Enhanced Student Performance Tracking","details":"With our school management system, educators can easily track and analyze student performance. They can record grades, generate progress reports, and identify areas where students may need additional support"},{"title":"Improved Parent Engagement","details":"Parents can access Infinity to stay informed about their child's academic progress, attendance, and upcoming events through our mobile application. They can communicate with teachers, view assignments, and participate in parent-teacher"}]},"headers":[],"relativePath":"index.md","filePath":"index.md"}`
  ),
  n = { name: 'index.md' };
function s(i, o, r, c, d, m) {
  return t(), a('div');
}
const g = e(n, [['render', s]]);
export { u as __pageData, g as default };
