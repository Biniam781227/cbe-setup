//importing routes
import systemAdminRoute from '../src/routes/admin/systemAdminRoute'

import logRoute from '../src/routes/common/logRoute'
import dashboardRoute from '../src/routes/common/dashboardRoute'
import roleRoute from '../src/routes/common/roleRoute'
import permissionRoute from '../src/routes/common/permissionRoute'
import dateRoute from '../src/routes/common/dateRoute'
import messageRoute from '../src/routes/common/messageRoute'
import streamLogRoute from '../src/routes/common/streamLogRoute'
import notificationRoute from '../src/routes/common/notificationRoute'

//regional routes
import regionalAdminRoute from '../src/routes/regional/admin/regionalAdminRoute'
import regionalEducationOfficialRoute from '../src/routes/regional/officials/regionalEducationOfficialRoute'
// import regionalCurriculum from '../src/routes/regional/regionalCurriculumRoute'
import courseCurriculum from '../src/routes/course_author/courseCurriculumRoute'
import curriculumCategory from '../src/routes/course_author/curriculumCategoryRoute'
import courseContent from '../src/routes/course_author/courseContentRoute'
import contentLog from '../src/routes/course_author/contentLogRoute'
import regionalEducation from '../src/routes/regional/regionalEducationRoute'
import workingPathRoute from '../src/routes/regional/workingPathRoute'
import personalityTest from '../src/routes/regional/personalityTestRoute'
import controlPoint from '../src/routes/regional/regionalControlPointRoute'
// import regionalTestBankManual from '../src/routes/regional/regionalTestBankManualRoute'
// import regionalTestBank from '../src/routes/regional/regionalTestBankRoute'
// import regionalQuestionBank from '../src/routes/regional/regionalQuestionBankRoute'
import Library from '../src/routes/library/libraryRoute'
import LibCategory from '../src/routes/library/libCategoryRoute'
import LibActivity from '../src/routes/library/libActivityRoute'
import LibRating from '../src/routes/library/libRatingRoute'
import LibReview from '../src/routes/library/libReviewRoute'
import Lab from '../src/routes/lab/labRoute'
import LabCategory from '../src/routes/lab/labCategoryRoute'
import LabActivity from '../src/routes/lab/labActivityRoute'
import LabRating from '../src/routes/lab/labRatingRoute'
import LabReview from '../src/routes/lab/labReviewRoute'
import regionalExaminationResult from '../src/routes/regional/regionalExaminationResultRoute'
import channelsRoute from '../src/routes/course_author/channelsRoute'
import organizationRoute from '../src/routes/course_author/organizationRoute'
import transactionRoute from '../src/routes/course_author/transactionRoute'

//school routes
import schoolAdminRoute from '../src/routes/school/schoolAdminRoute'
import schoolOfficialsRoute from '../src/routes/school/schoolOfficialsRoute'
// import schoolCurriculum from '../src/routes/school/schoolCurriculumRoute'
// import schoolCourseContent from '../src/routes/school/schoolCourseContentRoute'
// import schoolCompetition from '../src/routes/school/schoolCompetitionRoute'
// import schoolClass from '../src/routes/school/schoolClassRoute'
import items from '../src/routes/school/itemsRoute'
import manualItemsRoute from '../src/routes/school/manualItemsRoute'
import schoolProfile from '../src/routes/school/schoolProfileRoute'
import schoolConfig from '../src/routes/school/schoolConfigRoute'
// import schoolTestBank from '../src/routes/school/schoolTestBankRoute'
// import schoolQuestionBank from '../src/routes/school/schoolQuestionBankRoute'
// import schoolReward from '../src/routes/school/schoolRewardRoute'
import teacherProfile from '../src/routes/school/teacherProfileRoute'
import manualAssessment from '../src/routes/school/school_management/mark/manualAssessmentRoute'
import manualAssessmentResult from '../src/routes/school/school_management/mark/manualAssessmentResultRoute'
// import curriculumSell from '../src/routes/school/schoolCurriculumSellRoute'
import academicYearRoute from '../src/routes/school/school_management/academicYearRoute'
import announcementRoute from '../src/routes/school/school_management/announcementRoute'
import classRoute from '../src/routes/school/school_management/classRoute'
import sectionRoute from '../src/routes/school/school_management/sectionRoute'
import studentSectionRoute from '../src/routes/school/school_management/studentSectionRoute'
import teacherCourseRoute from '../src/routes/school/school_management/teacherCourseRoute'
import eventRoute from '../src/routes/school/school_management/eventRoute'
import scheduleRoute from '../src/routes/school/school_management/scheduleRoute'
import sessionRoute from '../src/routes/school/school_management/sessionRoute'
import attendanceRoute from '../src/routes/school/school_management/attendanceRoute'

// import studentPerformance from '../src/routes/school/studentPerformanceRoute'
import studentPerformance from '../src/routes/student/studentPerformance/studentPerformanceRoute'
import studentOverallStatus from '../src/routes/student/studentPerformance/studentOverallStatusRoute'
// import studentRegionalExam from '../src/routes/student/studentPerformance/studentRegionalExamRoute'
import studentItem from '../src/routes/student/studentPerformance/studentItemRoute'

//student
import personalityAssessment from '../src/routes/student/personalityAssessmentRoute'
import studentProfile from '../src/routes/student/studentProfileRoute'

//parent  parentProfile
import parentProfile from '../src/routes/parent/parentProfileRoute'

import feeRoute from '../src/routes/school/school_management/fee/feeRoute'

import paymentAmountRoute from '../src/routes/school/school_management/fee/paymentAmount'

import groupRoute from '../src/routes/school/school_management/fee/group'

import payerRoute from '../src/routes/school/school_management/fee/payer'

import paymentTermRoute from '../src/routes/school/school_management/fee/paymentTerm'

import paymentTypeRoute from '../src/routes/school/school_management/fee/paymentTypes'

import discountRoute from '../src/routes/school/school_management/fee/discount'

import billsRoute from '../src/routes/school/school_management/fee/bills'

import academicSectionRoute from '../src/routes/school/school_management/academicSection'

import subjectRoute from '../src/routes/school/school_management/subject'

import billingRoute from '../src/routes/common/billingRoute.js'

import importStudentsRoute from '../src/routes/migration/importStudents'

import moduleRoute from '../src/routes/school/permission/module'

import submoduleRoute from '../src/routes/school/permission/submodule'

import actionRoute from '../src/routes/school/permission/action'

export default (app) => {
    app.use('/api/common/date', dateRoute)
    app.use('/api/role', roleRoute)
    app.use('/api/log', logRoute)
    app.use('/api/dashboard', dashboardRoute)
    app.use('/api/permission', permissionRoute)
    app.use('/api/message', messageRoute)
    app.use('/api/stream', streamLogRoute)
    app.use('/api/notification', notificationRoute)

    //applying routes
    app.use('/api/admin', systemAdminRoute)
    app.use('/api/reb/admin', regionalAdminRoute)
    app.use('/api/reb/official', regionalEducationOfficialRoute)
    app.use('/api/reb/regionalEducation', regionalEducation)
    // app.use('/api/reb/curriculum', regionalCurriculum)
    app.use('/api/reb/courseCurriculum', courseCurriculum)
    app.use('/api/reb/curriculumCategory', curriculumCategory)
    app.use('/api/reb/courseContent', courseContent)
    app.use('/api/reb/contentLog', contentLog)
    app.use('/api/reb/workingPaths', workingPathRoute)
    app.use('/api/reb/personalitytest', personalityTest)
    app.use('/api/reb/controlPoint', controlPoint)
    // app.use('/api/reb/regionalTestBankManual', regionalTestBankManual)
    // app.use('/api/reb/regionalTestBank', regionalTestBank)
    // app.use('/api/reb/regionalQuestionBank', regionalQuestionBank)
    app.use('/api/reb/library', Library)
    app.use('/api/reb/libCategory', LibCategory)
    app.use('/api/reb/libActivity', LibActivity)
    app.use('/api/reb/libRating', LibRating)
    app.use('/api/reb/libReview', LibReview)
    app.use('/api/reb/lab', Lab)
    app.use('/api/reb/labCategory', LabCategory)
    app.use('/api/reb/labActivity', LabActivity)
    app.use('/api/reb/labRating', LabRating)
    app.use('/api/reb/labReview', LabReview)
    app.use('/api/reb/regionalExaminationResult', regionalExaminationResult)
    app.use('/api/reb/channel', channelsRoute)
    app.use('/api/reb/organization', organizationRoute)
    app.use('/api/reb/transaction', transactionRoute)

    // applying school routes
    app.use('/api/school/admin', schoolAdminRoute)
    app.use('/api/school/official', schoolOfficialsRoute)
    // app.use('/api/school/curriculum', schoolCurriculum)
    // app.use('/api/school/courseContent', schoolCourseContent)
    // app.use('/api/school/competition', schoolCompetition)
    // app.use('/api/school/schoolClass', schoolClass)
    app.use('/api/school/items', items)
    app.use('/api/school/manualItems', manualItemsRoute)
    app.use('/api/school/schoolProfile', schoolProfile)
    app.use('/api/school/config', schoolConfig)
    // app.use('/api/school/schoolTestBank', schoolTestBank)
    // app.use('/api/school/schoolQuestionBank', schoolQuestionBank)
    // app.use('/api/school/schoolReward', schoolReward)
    app.use('/api/school/teacherProfile', teacherProfile)
    app.use('/api/school/manualAssessment', manualAssessment)
    app.use('/api/school/manualAssessmentResult', manualAssessmentResult)
    // app.use('/api/school/curriculumSell', curriculumSell)
    app.use('/api/school/academicYear', academicYearRoute)
    app.use('/api/school/announcement', announcementRoute)
    app.use('/api/school/class', classRoute)
    app.use('/api/school/section', sectionRoute)
    app.use('/api/school/academicSection', academicSectionRoute)
    app.use('/api/school/studentSection', studentSectionRoute)
    app.use('/api/school/teacherCourse', teacherCourseRoute)
    app.use('/api/school/event', eventRoute)
    app.use('/api/school/schedule', scheduleRoute)
    app.use('/api/school/session', sessionRoute)
    app.use('/api/school/attendance', attendanceRoute)
    // app.use('/api/school/calendar', calendarRoute)
    // app.use('/api/school/studentPerformance', studentPerformance)

    //student
    app.use('/api/student/personalityAssessment', personalityAssessment)
    app.use('/api/student/studentProfile', studentProfile)
    app.use('/api/student/studentPerformance', studentPerformance)
    app.use('/api/student/studentOverallStatus', studentOverallStatus)
    app.use('/api/student/studentItem', studentItem)

    //parent
    app.use('/api/parent/parentProfile', parentProfile)
    app.use('/api/school/fee', feeRoute)
    app.use('/api/school/paymentAmount', paymentAmountRoute)
    app.use('/api/school/group', groupRoute)
    app.use('/api/school/payer', payerRoute)
    app.use('/api/school/paymentTerm', paymentTermRoute)
    app.use('/api/school/paymentType', paymentTypeRoute)
    app.use('/api/school/discount', discountRoute)
    app.use('/api/school/bills', billsRoute)
    app.use('/api/school/subject', subjectRoute)

    app.use('/api/billing', billingRoute)
    app.use('/api/migration', importStudentsRoute)
    app.use('/api/school/module', moduleRoute)
    app.use('/api/school/submodule', submoduleRoute)
    app.use('/api/school/action', actionRoute)
}
