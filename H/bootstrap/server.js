import http from 'http'
// import https from 'https'
import socketio from 'socket.io'
import express from 'express'
import path from 'path'
import cors from 'cors'
import fs from 'fs'
import morgan from 'morgan'
import setRoutes from './routes'
import rateLimit from 'express-rate-limit'
import helmetEncapsulation from './helmet'
// import setGraphql from './graphql'
// import cluster from "cluster";
// import os from 'os';
import dotenv from 'dotenv'
import config from 'config'
// import initRedisSocket from './redis'
// import { createClient } from 'redis'
// import redisAdapter from 'socket.io-redis'
// const pubClient = createClient({ url: 'redis://localhost:6379' })
// const subClient = pubClient.duplicate()
// import redisClient from './redis'
// const redisClient = initRedisClient()

// mongo sanitize
const mongoSanitize = require('express-mongo-sanitize')

// toobusy
const toobusy = require('toobusy-js')

// HTTP Parameter Pollution attacks
const hpp = require('hpp')

// load the environment variables
dotenv.config()

// import startup from '../src/jobs/startup'
// const { ExpressPeerServer } = require('peer')

const server = express()
/* server.use('/api/test', (req, res, next) => {
    for (let i = 0; i < 10000000000; i++) {}
    return res.send('task finished')
})
 */
const accessLogStream = fs.createWriteStream(
    path.normalize(__dirname + '/../logs/access.log'),
    {
        flags: 'a',
    }
)

// server.use(
//   morgan('combined', {
//     stream: accessLogStream
//   })
// )

server.use(
    morgan(function (tokens, req, res) {
        const message = [
            tokens.method(req, res),
            tokens.url(req, res),
            tokens.status(req, res),
            tokens.res(req, res, 'content-length'),
            '-',
            tokens['response-time'](req, res),
            'ms',
        ].join(' ')
        // UDP_CONNECTION.send(message, 0, message.length, config.get('s_port'), config.get('s_ip'));
        return message
    })
)

// rate limiter
const limiter = rateLimit({
    windowMs: 1 * 60 * 10000,
    max: 100,
    standardHeaders: true,
    legacyHeaders: false,
})

// server.use(limiter)

helmetEncapsulation(server)

server.use(morgan('dev'))

server.use(
    cors({
        origin: config.get('allowed_urls'),
    })
)

server.use(
    express.json({
        limit: '3000kb',
    })
)
server.use(express.static('uploads'))

// MONGO SANITIZE
server.use(mongoSanitize())

// toobusy
// server.use(function (req, res, next) {
//     if (toobusy()) {
//         res.status(503).send('Server busy. Please be patient!')
//     } else {
//         next()
//     }
// })

// HTTP Parameter Pollution attacks
server.use(hpp())

// HTTP Parameter Pollution attacks

// server.use(
//     express.static('uploads',
//          {
//             setHeaders: function (res, path, stat) {
//                 res.set('Access-Control-Allow-Origin', '*')
//             }
//         }
//     )
// )
// server.use("/api/images", express.static("storage/images"));
// server.use("/api/files", express.static("storage/files"));
setRoutes(server)
// setGraphql(server)

server.use((error, req, res, next) => {
    res.send(error.message)
})

// console.log(os.cpus().length);
// if(cluster.isMaster){
//   for(let i=0;i<os.cpus().length;i++){
//     const worker=cluster.fork();
//     worker.on('exit',()=>cluster.fork());
//     worker.on('error',()=>cluster.fork());
//   }
// } else

// const app = https.createServer({
//         key: fs.readFileSync(path.normalize(__dirname + '/../keys/server-key.pem')),
//         cert: fs.readFileSync(
//             path.normalize(__dirname + '/../keys/server-cert.pem')
//         )
//     },
//     server
// )

const app = http.createServer(server)
// initRedisSocket(app)

// const peerServer = ExpressPeerServer(app, {
//         debug: true,
//         path: '/',
//         allow_discovery: true
//     })
// console.log("peerServer", peerServer);

// server.use('/', peerServer)

// peerServer.on('connection', client => {
//     console.log('PEER-CONNECTED', client.id)
// })

// peerServer.on('disconnect', client => {
//     console.log('PEER-DISCONNECTED', client.id)
// })
//
// 404 interceptor
server.use('*', (req, res, next) => {
    res.status(404).send({ status: 'FAIL', message: 'Unregistered route' })
})

// // const io = socketio(app)
// const io = socketio(app, {
//     cors: {
//         origin: config.get('allowed_urls'),
//         methods: ['GET', 'POST'],
//         allowedHeaders: ['token'],
//         credentials: true,
//         allowEIO3: true,
//     },
// })
// io.adapter(redisAdapter({ pubClient, subClient }))
// io.use(socket_middleware)
// io.on('connect', (socket) => {
//     console.info('socket ', socket.id)
//     handleSocketConnection(socket)
//     // Store the socket ID in Redis
//     // Listen for socket events
//     socket.on('disconnect', () => {
//         console.info('disconnected ', socket.id)
//         // client.del(`socket:${socket.id}`)
//     })
// })
// global.io = io
// EXECUTE STARTUP PROCESSES())
// startup()

export { app }
